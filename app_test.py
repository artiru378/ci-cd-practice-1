import unittest
from unittest.mock import patch, MagicMock
from app import app


class TestApp(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    @patch('app.redis.StrictRedis')
    def test_search(self, mock_redis):
        # Mocking the redis client and its methods
        mock_redis_instance = MagicMock()
        mock_redis_instance.exists.return_value = True
        mock_redis_instance.lrange.return_value = [1, 2, 3, 4, 5]
        mock_redis.return_value = mock_redis_instance
        search_value = 42  # Update search value
        response = self.app.post('/search', json={'value': search_value})
        data = response.get_json()
        self.assertEqual(response.status_code, 200)
        self.assertIn('index', data)
        if search_value not in [1, 2, 3, 4, 5]:
            expected_index = -1
        else:
            expected_index = [1, 2, 3, 4, 5].index(search_value)
        self.assertEqual(data['index'], expected_index)


if __name__ == '__main__':
    unittest.main()