from flask import Flask, request, jsonify
import redis
import random

app = Flask(__name__)
redis_client = redis.StrictRedis(host='redis', port=6379, db=0)


@app.route('/search', methods=['POST'])
def search():
    data = request.get_json()
    search_value = int(data['value'])
    array = get_or_create_array()
    result = binary_search(array, search_value)
    return jsonify({'index': result})


def get_or_create_array():
    if redis_client.exists('array'):
        return list(map(int, redis_client.lrange('array', 0, -1)))
    else:
        array = sorted([random.randint(1, 100) for _ in range(100)])
        redis_client.rpush('array', *array)
        return array


def binary_search(arr, x):
    left, right = 0, len(arr) - 1
    while left <= right:
        mid = left + (right - left) // 2
        if arr[mid] == x:
            return mid
        elif arr[mid] < x:
            left = mid + 1
        else:
            right = mid - 1
    return -1


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
