import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_search_endpoint(client):
    response = client.post('/search', json={'value': 5})
    assert response.status_code == 200
    # Check if 'index' key exists in the response
    assert 'index' in response.json
    response = client.post('/search', json={'value': 11})
    assert response.status_code == 200
    assert 'index' in response.json
    response = client.post('/search', json={'value': 0})
    assert response.status_code == 200
    assert 'index' in response.json