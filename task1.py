# Import necessary modules
import sys
import random


# Define the main function
def main():
    # Generate a sorted array of random numbers
    array = sorted([random.randint(1, 100) for _ in range(100)])
    # Get the user input for the search value
    if len(sys.argv) != 2:
        print("Usage: python task1.py <search_value>")
        sys.exit(1)
    search_value = sys.argv[1]
    try:
        search_value = int(search_value)
    except ValueError:
        print("Search value must be an integer")
        sys.exit(1)
    if not 1 <= search_value <= 1000:
        print("Search value must be between 1 and 1000")
        sys.exit(1)
    # Perform binary search
    result = binary_search(array, search_value)
    # Print the result
    if result != -1:
        print(f"Value {search_value} found at index {result}")
    else:
        print(f"Value {search_value} not found in the array")


# Define the binary search function
def binary_search(arr, x):
    # Initialize pointers for binary search
    left = 0
    right = len(arr) - 1
    # Binary search algorithm
    while left <= right:
        mid = left + (right - left) // 2
        if arr[mid] == x:
            return mid
        elif arr[mid] < x:
            left = mid + 1
        else:
            right = mid - 1
    return -1


# Call the main function if the script is executed directly
if __name__ == "__main__":
    main()
